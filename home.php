<?php
	session_start();
	if($_SESSION['isLogin'] != true || $_SESSION['jam_selesai']==date("Y-m-d H:i:s"))
	{
		header("Location: form_login.php?message=nologin");
	}
	echo "Selamat datang, ",strtoupper($_SESSION['uname'])," Anda login pada: ",$_SESSION['jam_mulai'];
	echo "<br>";
?>
	<a href="logout.php">Logout</a>

<DOCTYPE html>
<html>
<body>
	<br>
	<h2>Form Pendataan :</h2>

	<form action="simpandata.php" method="POST">
	<label>Nama Lengkap :</label>
    <input name="nama" type="text">

	<br><br>
	<label>Username :</label>
	<?php
		echo($_SESSION['uname']);
	?>

	<br><br>
	<label>Alamat :</label>
	<textarea name="alamat" cols="30" rows="4"> </textarea>
	

	<br><br>
	<label>Jenis Kelamin</label><br>
	<input type="radio" checked="checked" name="jenis_kelamin" value="pria">Pria<br>
	<input type="radio" name="jenis_kelamin" value="wanita">Wanita<br>


	<br><br>
	<label>Hobi :</label>
	<p><input type='checkbox' name='hobi[]' value="Membaca Buku"/>Membaca Buku</p>
    <p><input type='checkbox' name='hobi[]' value="Mendengarkan Musik"/>Mendengarkan Musik</p>
    <p><input type='checkbox' name='hobi[]' value="Bermain Game Valorant" />Bermain Game Valorant</p>
	<p><input type='checkbox' name='hobi[]' value="Olahraga" />Olahraga</p>

	<br><br>
	<select name="pekerjaan">
	  <option value="Dokter">Dokter</option>
	  <option value="Wiraswasta">Wiraswasta</option>
      <option value="Guru">Guru</option>
      <option value="Atlet">Atlet</option>
      <option value="Koki">Koki</option>
      <option value="Tentara">Tentara</option>
      <option value="Polisi">Polisi</option>
      <option value="Karyawan Swasta">Karyawan Swasta</option>
	  <option value="Insinyur">Insinyur</option>
	  <option value="Belum Bekerja">Belum Bekerja</option>
	  <option value="Lainnya">Lainnya</option>
    </select>

	<br><br>
	<input type="reset" value="Reset">
	<input type="submit" value="Simpan" action="simpandata.php">
	</form>
</body>
</html>
	<!-- 
		tugas 2:
		tambahkan form yang method-nya post ke halaman simpandata.php
		nama lengkap: text
		username : ambil dari session
		alamat: textarea
		jenis kelamin: radio, walaupun pilihannya 2, name tetap 1
		hobi: checkbox -> diisi terserah minimal 3 pilihan, pilihannya 3, name juga 3
		pekerjaan: select -> diisi terserah minimal 3 pilihan, sama kayak radio

		tombol simpan


		kalo sudah klik simpan, tampilkan hasil inputannya
	-->
	